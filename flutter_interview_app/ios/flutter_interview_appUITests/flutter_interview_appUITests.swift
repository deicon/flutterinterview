//
//  flutter_interview_appUITests.swift
//  flutter_interview_appUITests
//
//  Created by Dieter Eickstädt on 11.05.22.
//

import XCTest

class flutter_interview_appUITests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()
        
        let tab2Of2StaticText = app.staticTexts["Tab 2 of 2"]
        tab2Of2StaticText.tap()
        app.buttons["Party"].tap()
        app.buttons["Bunny"].tap()
        app.buttons["Fun"].tap()
        app.staticTexts["Tab 1 of 2"].tap()
        tab2Of2StaticText.tap()
        
        let game8CasinoFunBunnyHolidayStaticText = app.staticTexts["Game 8 [Casino, Fun, Bunny, Holiday]"]
        game8CasinoFunBunnyHolidayStaticText.tap()
        game8CasinoFunBunnyHolidayStaticText/*@START_MENU_TOKEN@*/.swipeRight()/*[[".swipeUp()",".swipeRight()"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/

        
                        
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, watchOS 7.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}
