# flutter_interview_app

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

# Questions
- Introducing Ously (Jens)
- What we are looking for
  - Flutter App in Production
  - Complex Distributed Java backend 
  - Dev Team Setup
    - 1 App Dev
    - 1 Bckend Dev
    - 1 Frontend Dev
- Tell us something about your experiences (Flutter, Mobile Dev, General Development)



 https://gitlab.com/deicon/flutterinterview

 https://gitlab.com/deicon/flutterinterview/-/tree/develop/assignments/state