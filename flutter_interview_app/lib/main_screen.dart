import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_interview_app/centered_text_image.dart';

import 'animated_text.dart';
import 'list_view.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {

  @override
  Widget build(BuildContext context) {

    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          bottom: const TabBar(
              tabs: [
                Tab(icon: Icon(Icons.message),),
                Tab(icon: Icon(Icons.event),),
                Tab(icon: Icon(Icons.dangerous),),
              ]
          ),
        ),
        body: TabBarView(children: [
          AnimatedTextView(),
          const GameList(),
          CenteredTextImage()
        ]),
      ),
    );
  }
}
