import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_interview_app/games.dart';

class GameList extends StatelessWidget {
  const GameList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var allGames = allAvailableGames();

    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Flexible(
          flex: 2,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
              children: [
            Expanded(
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: allcategories.length,
                itemBuilder: (context, index) => Padding(
                  padding: EdgeInsets.fromLTRB(5, 2, 5, 0),
                  child: Center(
                    child: ElevatedButton(
                      onPressed: () {  },
                      child: Text(
                        allcategories.elementAt(index),
                      ),
                    ),
                  ),
                ),
              ),
            )
          ]),
        ),
        Flexible(
          flex: 30,
          child: ListView.builder(
            itemCount: allGames.length,
            itemBuilder: (context, index) => ListTile(
              leading: const Icon(Icons.gamepad_sharp),
              title: Text("${allGames.elementAt(index).name} \n [${allGames.elementAt(index).categories.join(", ")}]"),
            ),
          ),
        ),
      ],
    );
  }
}
