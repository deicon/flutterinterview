import 'dart:math' as math;
extension Radians on double {
  double radians() {
    return this * (math.pi / 180.0);
  }
}