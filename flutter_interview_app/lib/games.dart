import 'dart:math' as math;

class Game {
  final String name;
  final String id;
  final List<String> categories;

  const Game(this.id, this.name, this.categories);
}

const allcategories = {
  "Fun",
  "Party",
  "Bunny",
  "Holiday",
  "Football",
  "Freaky",
  "Casino"
};

List<Game> allAvailableGames() {
  return List.generate(1000,
      (index) => Game("$index", "Game $index", getRandomCategories().toList()));
}

Set<String> getRandomCategories() {
  return List.generate(
          math.Random.secure().nextInt(allcategories.length),
          (index) =>
              allcategories.elementAt(math.Random.secure().nextInt(allcategories.length)))
      .toSet();
}
