# Requirement
The Customer wants the user to filter within a List of 
Games by one Category. Games do have a number of categories. 
By clicking one of the top category buttons, the list should only
render games with at least this category or no category at all. 

1. One Filter narrows list entries
2. Current Filter stands out visually
3. Optionally multiple filters might be selectable

<img src="ListFilter.gif" alt="drawing" width="200"/>

<video width=200 controls="true" src="ListFilter.mp4" type="video/mp4" >