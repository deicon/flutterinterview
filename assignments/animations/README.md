# Requirement
The Customer wants an animated Text

1. Gradually becoming visible
2. "Flying in" from top of the screen
3. For better attention rotating into its final center position

<img src="AnimatedText.gif" alt="drawing" width="200"/>

<video width=200 controls="true" src="AnimatedText.mp4" type="video/mp4" >